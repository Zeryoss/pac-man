package com.pacman.game.tools;

import java.util.Comparator;

public class ComparativeHeuristics implements Comparator<Node> {

	@Override
	public int compare(Node arg0, Node arg1) {
		if(arg0.getCoutTotal() < arg1.getCoutTotal())
			return -1;
		else
			return 1;
	}

}
