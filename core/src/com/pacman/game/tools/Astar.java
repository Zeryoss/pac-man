package com.pacman.game.tools;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import com.pacman.game.model.Ghosts;
import com.pacman.game.model.GameElement;
import com.pacman.game.model.Pacman;
import com.pacman.game.view.WorldRenderer;

public class Astar {
	public static Node astar(int[][] tab, Node dep, Node fin, Ghosts fe) {
		LinkedList<Node> lifo = new LinkedList<Node>();
		Comparator<Node> comp = new ComparativeHeuristics();
		PriorityQueue<Node> openList = new PriorityQueue<Node>(20, comp);
		ArrayList<Node> closedList= new ArrayList<Node>();
		openList.add(dep);
		dep.setHeuristique(fin);
		Node u = null;
		boolean ok = false;
		while(!ok && !openList.isEmpty()){
			u = openList.poll();
			closedList.add(u);

			ArrayList<Node> voisin;
				if(fe.isDead)
					voisin = voisinDEAD(tab,fin, u);
				else
					voisin = voisin(tab,fin, u);

			for (Node succ : voisin) {
				if(u.getX() == fin.getX() && u.getY() == fin.getY()){
					ok = true;
				}
				else if(!succ.estDansOpenUClose(openList, closedList) || succ.getCout() > u.getCout() + 1 ){
					succ.setCout(u.getCout()+1);
					succ.setCoutTotal(succ.getCout() + succ.getHeuristique());
					openList.add(succ);
				}
				else if (succ.estDansClose(closedList))
					closedList.add(succ);
			}
		}
		if (u == null)
			return dep;

		if(u != null){
			while(u.getPere() != null){
				lifo.addFirst(u);
				u = u.getPere();
				
			}
		}
		return lifo.pollFirst();
	}
	
	private static ArrayList<Node> voisin(int lab[][], Node fin,Node u){
		ArrayList<Node> voisin = new ArrayList<Node>();
		Node n;
		
		if(u.getY()-1 >= 0 && u.getY() +1 < 28 && u.getX()-1 >= 0 && u.getX()+1 < 31){
			if(lab[u.getX()][u.getY()-1] != 1 && lab[u.getX()][u.getY()-1] != 2){
				n = new Node(u.getX(),u.getY()-1);
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()-1][u.getY()] != 1 && lab[u.getX()-1][u.getY()] != 2){
				n = new Node(u.getX()-1,u.getY());
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()+1][u.getY()] != 1 && lab[u.getX()+1][u.getY()] != 2){
				n = new Node(u.getX()+1,u.getY());
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()][u.getY()+1] != 1 && lab[u.getX()][u.getY()+1] != 2){
				n = new Node(u.getX(),u.getY()+1);
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
		}
		return voisin;
		
	}
	
	private static ArrayList<Node> voisinDEAD(int lab[][], Node fin,Node u){
		ArrayList<Node> voisin = new ArrayList<Node>();
		Node n;
		
		if(u.getY()-1 >= 0 && u.getY() +1 < 28 && u.getX()-1 >= 0 && u.getX()+1 < 31){
			if(lab[u.getX()][u.getY()-1] != 1){
				n = new Node(u.getX(),u.getY()-1);
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()-1][u.getY()] != 1){
				n = new Node(u.getX()-1,u.getY());
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()+1][u.getY()] != 1){
				n = new Node(u.getX()+1,u.getY());
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
			if(lab[u.getX()][u.getY()+1] != 1){
				n = new Node(u.getX(),u.getY()+1);
				n.setHeuristique(fin);
				n.setPere(u);
				voisin.add(n);
			}
		}
		return voisin;
		
	}
	
	public static int roundPosY(float x, GameElement ge){
		if(x % WorldRenderer.ppuX == 0)
			return (int)(x);
		else{
			if(ge instanceof Pacman){
				Pacman pm = (Pacman) ge;
				if(pm.isRightMove()){
					return (int) (x + (WorldRenderer.ppuX- (x % WorldRenderer.ppuX)));
				}
				else if(pm.isLeftMove()){
					return (int) (x - (x % WorldRenderer.ppuX));
				}
			}
			else if(ge instanceof Ghosts){
				Ghosts f = (Ghosts) ge;
				if(f.isRightMove()){
					return (int) (x + (WorldRenderer.ppuX- (x % WorldRenderer.ppuX)));
				}
				else if(f.isLeftMove()){
					return (int) (x - (x % WorldRenderer.ppuX));
				}
			}
		}
		return 0;
	}

	public static int roundPosX(float y, GameElement ge){
		if(y % WorldRenderer.ppuX == 0)
			return (int)(y);
		else{
			if(ge instanceof Pacman){
				Pacman pm = (Pacman) ge;
				if(pm.isUpMove()){
					return (int) (y + (WorldRenderer.ppuX- (y % WorldRenderer.ppuX)));
				}
				else if(pm.isDownMove()){
					return (int) (y - (y % WorldRenderer.ppuX));
				}
			}
			else if(ge instanceof Ghosts){
				Ghosts f = (Ghosts) ge;
				if(f.isUpMove()){
					return (int) (y + (WorldRenderer.ppuX- (y % WorldRenderer.ppuX)));
				}
				else if(f.isDownMove()){
					return (int) (y - (y % WorldRenderer.ppuX));
				}
			}
		}
		return 0;
	}
}
