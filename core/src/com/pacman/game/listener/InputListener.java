package com.pacman.game.listener;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.pacman.game.model.World;
import com.pacman.game.screens.LoseScreen;

public class InputListener implements InputProcessor {

	private World world;
	private static float R = (float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth();
	public static int actualKey = Input.Keys.LEFT, oldKey = Input.Keys.LEFT;
	public static boolean directionChanged = true;

	public InputListener(World world) {
		this.world = world;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.SPACE)
			((Game) Gdx.app.getApplicationListener()).setScreen(new LoseScreen());
		if (actualKey != keycode && (keycode == Input.Keys.LEFT || keycode == Input.Keys.RIGHT
				|| keycode == Input.Keys.UP || keycode == Input.Keys.DOWN)) {
			if (oldKey != keycode) {
				if (directionChanged) {
					oldKey = actualKey;
					directionChanged = false;
				}
			}
			actualKey = keycode;
			switch (oldKey) {
			case Input.Keys.LEFT:
				world.getPacman().setLeftMove(true);
				break;
			case Input.Keys.RIGHT:
				world.getPacman().setRightMove(true);
				break;
			case Input.Keys.DOWN:
				world.getPacman().setDownMove(true);
				break;
			case Input.Keys.UP:
				world.getPacman().setUpMove(true);
				break;
			default:
				break;
			}
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchDirection(screenX, screenY);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public void touchDirection(int screenX, int screenY) {
		boolean isAboveD1 = isAboveFirstDiagonal(screenX, screenY);
		boolean isAboveD2 = isAboveSecondDiagonal(screenX, screenY);

		if (isAboveD1 && isAboveD2) {
			keyDown(Input.Keys.DOWN);
		} else if (isAboveD1 && !isAboveD2) {
			keyDown(Input.Keys.LEFT);
		} else if (!isAboveD1 && isAboveD2) {
			keyDown(Input.Keys.RIGHT);
		} else {
			keyDown(Input.Keys.UP);
		}
	}

	public boolean isAboveFirstDiagonal(int x, int y) {
		return (float) y - (R * (float) x) > 0.0;
	}

	public boolean isAboveSecondDiagonal(int x, int y) {
		return (float) y - (-R * (float) x + (float) Gdx.graphics.getHeight()) > 0.0;
	}

}
