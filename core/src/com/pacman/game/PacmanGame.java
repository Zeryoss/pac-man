package com.pacman.game;

import com.badlogic.gdx.Game;
import com.pacman.game.screens.LaunchScreen;

public class PacmanGame extends Game {
		
	public void create(){
		setScreen(new LaunchScreen());
	}
}
