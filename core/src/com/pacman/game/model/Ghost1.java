package com.pacman.game.model;

import java.util.Random;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.pacman.game.view.TextureFactory;

public class Ghost1 extends Ghosts {

	public Ghost1(Vector2 position, World world) {
		super(position, world);
		leftMove = true;
		leftHome = true;
	}

	public TextureRegion getTexture(){
		return TextureFactory.iTexturable(this);
	}

	public void update(float delta){
		if(isDead){
			backToHome(delta);
		}
		else if(!leftHome){
			leaveHome(delta);
		}
		else{
			if(leftMove){
				MoveDirection(delta);
			}
			else if(rightMove){
				MoveDirection(delta);
			}
			else if(upMove){
				MoveDirection(delta);
			}
			else if(downMove){
				MoveDirection(delta);
				
			}
		}
	}

	private void MoveDirection(float delta) {
		Random r = new Random();
		int value;
		boolean TryOk = false;
		do{
			value = 0 + r.nextInt(4 - 0);
			switch(value){
			case 0 : 
				if(!rightMove && tryLeftMove(delta))
					TryOk = true;
				break;
			case 1 : 
				if(!leftMove && tryRightMove(delta))
					TryOk = true;
				break;
			case 2 :
				if(!downMove && tryUpMove(delta))
					TryOk = true;
				break;
			case 3 : 
				if(!upMove && tryDownMove(delta))
					TryOk = true;
				break;
			}
		}while(!TryOk);
		
		switch(value){
		case 0 : 
			rightMove = upMove = downMove = false;
			leftMove = true;
			leftMove(delta);
			break;
		case 1 : 
			leftMove = upMove = downMove = false;
			rightMove = true;
			rightMove(delta);
			break;
		case 2 : 
			rightMove = leftMove = downMove = false;
			upMove = true;
			upMove(delta);
			break;
		case 3 : 
			rightMove = upMove = leftMove = false;
			downMove = true;
			downMove(delta);
			break;
		}

	}

	@Override
	public void leaveHome(float delta) {
		pop(delta,0);	
	}
}
